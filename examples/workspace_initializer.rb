Mani.new(window_manager: :xmonad) do
  workspace 1 do
    window :atop, launch: 'termite' do
      run 'atop'
    end

    window :pavucontrol, launch: 'pavucontrol'
  end

  workspace 2 do
    window :firefox_email, launch: 'firefox-developer-edition -P email', delay: 1.5 do
      visit 'mail.zoho.com'
    end
  end

  workspace 3 do
    window :vim, launch: 'termite' do
      run 'nvim'
    end
  end

  workspace 4 do
    window :firefox_work, launch: 'firefox-developer-edition -P work', delay: 1.5 do
      visit 'outlook.office.com'

      browser_tab :new do
        visit 'teams.microsoft.com'
      end

      # Switch back to the first tab
      browser_tab 1
    end
  end

  workspace 8 do
    window :firefox, launch: 'firefox-developer-edition', delay: 1.5 do
      visit 'localhost:8080'

      browser_tab :new do
        visit 'https://discord.com/channels/@me'
      end

      # Switch back to the first tab
      browser_tab 1
    end
  end
end
