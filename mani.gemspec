$LOAD_PATH.unshift File.expand_path('../lib', __FILE__)
require 'mani/version'

Gem::Specification.new do |gem|
  gem.name          = 'mani'
  gem.homepage      = 'https://gitlab.com/xcip/mani'
  gem.license       = 'MIT'
  gem.summary       = 'A window automation tool'
  gem.description   = 'A window automation tool'
  gem.email         = 'xcip@protonmail.com'
  gem.authors       = ['xcip']

  gem.version       = Mani::Version::VERSION

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(spec)/})
  gem.require_paths = ['lib']

  gem.add_dependency 'x_do_bindings', '0.2.1'
end
