class Mani
  # This class holds the Mani version information.
  class Version
    # The current version
    VERSION = '0.0.7'
  end
end
